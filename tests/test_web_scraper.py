import sys
sys.path.append("..")
import unittest
from selenium import webdriver
import bs4 as bs
import pandas as pd
from classes import Web_Scraper

class Test_Web_Scraper(unittest.TestCase): 

    def __init__(self, *args, **kwargs):
        super(Test_Web_Scraper, self).__init__(*args, **kwargs)
        self.gdata = pd.read_csv("test_data/geo_data.csv",header=0)      
    
    def test_scrap_seloger(self):
        
        research_param = "zip_code"
        value = 95540
        offer_type = "location"

        f = open('test_data/seloger.html', 'r')
        html = f.read()
        f.close()

        soup = bs.BeautifulSoup(html,'html.parser')

        result = Web_Scraper.extract_seloger(self,self.gdata,research_param,offer_type,value,soup)

        expected = {
            "typeOffre":"location",
            "region":"ILE-DE-FRANCE",
            "departement":"VAL-D'OISE",
            "ville":"MERY SUR OISE",
            "typeBien":"Appartement",
            "prix":"808" ,
            "taille":"2",
            "superficie":"48"          
        }

        self.assertEqual(result,expected,"Error: extract_seloger")

    def test_scrap_laforet(self):
        
        research_param = "insee_code"
        value = 95607
        offer_type = "achat"

        f = open('test_data/laforet.html', 'r')
        html = f.read()
        f.close()

        soup = bs.BeautifulSoup(html,'html.parser')

        result = Web_Scraper.extract_laforet(self,self.gdata,research_param,offer_type,value,soup)

        expected = {
            "typeOffre":"achat",
            "region":"ILE-DE-FRANCE",
            "departement":"VAL-D'OISE",
            "ville":"TAVERNY",
            "typeBien":"Appartement",
            "prix":"225 000",
            "taille":"3",
            "superficie":"75"          
        }

        self.assertEqual(result,expected,"Error: extract_laforet")
    

if __name__ == '__main__':
    unittest.main()


