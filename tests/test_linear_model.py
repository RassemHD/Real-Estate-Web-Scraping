import sys
sys.path.append("..")
import unittest
import pandas as pd
import numpy as np 
from classes import Linear_Model

class Test_Linear_Model(unittest.TestCase): 

    def __init__(self, *args, **kwargs):
        super(Test_Linear_Model, self).__init__(*args, **kwargs)
        self.lm = Linear_Model("../data/cleaned_data.csv")   
    
    def test_x_prep(self):

        row = [["location","ILE-DE-FRANCE","VAL-D'OISE","ARGENTEUIL","Appartement",2,40.44,804.0]]
        
        test_pd = pd.DataFrame(row,columns=['typeOffre','region','departement','ville','typeBien','taille','superficie','prix'])
        expected = pd.DataFrame([["ILE-DE-FRANCE","VAL-D'OISE","ARGENTEUIL","Appartement",2,40.44]],columns = ["region","departement","ville","typeBien","taille","superficie"] )
        
        res = self.lm._x_prep(test_pd)

        self.assertTrue(expected.equals(res))
    
    
    def test_y_prep(self):

        row = [["location","ILE-DE-FRANCE","VAL-D'OISE","ARGENTEUIL","Appartement",2,40.44,804.0]]
        test_pd = pd.DataFrame(row,columns=['typeOffre','region','departement','ville','typeBien','taille','superficie','prix'])
        expected = pd.DataFrame([[804.0]],columns = ["prix"] )
        
        res = self.lm._y_prep(test_pd)
        res = pd.DataFrame(res,columns = ["prix"])

        self.assertTrue(expected.equals(res))


    def test_normalizer(self):

        row = [["ILE-DE-FRANCE","VAL-D'OISE","ARGENTEUIL","Appartement",2,40.44]]
        test_pd = pd.DataFrame(row,columns=['region','departement','ville','typeBien','taille','superficie'])
        
        expected = pd.DataFrame([[0,"VAL-D'OISE","ARGENTEUIL","Appartement",2,40.44]],columns = ['region','departement','ville','typeBien','taille','superficie'] )
        res = self.lm._normalizer(test_pd,"region")

        self.assertTrue(expected.equals(res))
    
    def test_data_prep(self):
        
        rows = [["location","ILE-DE-FRANCE","ESSONNE","GIF SUR YVETTE","Appartement",830.0,3,54.0],
           ["location","ILE-DE-FRANCE","VAL-DE-MARNE","GENTILLY","Appartement",757.0,1,np.nan]]
        test_pd = pd.DataFrame(rows,columns=['typeOffre','region','departement','ville','typeBien','prix','taille','superficie'])

        out = [["location",0,0,0,0,3,54.0,830.0]]
        expected = pd.DataFrame(out,columns= ["typeOffre","region","departement","ville","typeBien","taille","superficie","prix"])
        res = self.lm._data_prep(test_pd,"location")
        
        res = pd.DataFrame(res,columns = ["typeOffre","region","departement","ville","typeBien","taille","superficie","prix"])

        self.assertTrue(expected.equals(res))
          

if __name__ == '__main__':
    unittest.main()