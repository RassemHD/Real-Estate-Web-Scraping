import sys
sys.path.append("..")
import unittest
import pandas as pd
import numpy as np 
from classes import Data_Cleaner

class Test_Data_Cleaner(unittest.TestCase): 

    def __init__(self, *args, **kwargs):
        super(Test_Data_Cleaner, self).__init__(*args, **kwargs)
        self.cleaner = Data_Cleaner("test_data/test_clean_input.csv","")   
    
    def test_data_cleaning(self):
        
        # first cleaning ----------------------------------------

        rows = [
            ["location","ILE-DE-FRANCE","VAL-D'OISE","CERGY","Appartement","82 0 €","2c","2"],
            ["location","ILE-DE-FRANCE","VAL-D'OISE","PONTOISE","Maison","1100","1100","53,05 m²"]
        ]

        expected = pd.DataFrame(rows,columns=self.cleaner.data.columns)
        df1 = self.cleaner.first_cleaning()

        self.assertTrue(expected.equals(df1))

        # second cleaning ----------------------------------------

        rows = [
            ["location","ILE-DE-FRANCE","VAL-D'OISE","CERGY","Appartement",820.0,2,2.00],
            ["location","ILE-DE-FRANCE","VAL-D'OISE","PONTOISE","Maison",1100.0,1100,53.05]
        ]

        expected = pd.DataFrame(rows,columns=self.cleaner.data.columns)
        df2 = self.cleaner.second_cleaning(df1)

        self.assertTrue(expected.equals(df2))

        # third cleaning ----------------------------------------

        rows = [
            ["location","ILE-DE-FRANCE","VAL-D'OISE","CERGY","Appartement",820.0,2,np.nan],
            ["location","ILE-DE-FRANCE","VAL-D'OISE","PONTOISE","Maison",1100.0,np.nan,53.05]
        ]

        expected = pd.DataFrame(rows,columns=self.cleaner.data.columns)
        df3 = self.cleaner.third_cleaning(df2)
    
        #self.assertTrue(expected.equals(df3))
        

if __name__ == '__main__':
    unittest.main()