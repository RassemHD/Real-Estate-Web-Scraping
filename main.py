from classes import Web_Scraper,Data_Cleaner,Data_Processor,Data_Visualizer,Linear_Model
from selenium import webdriver
import logging

if __name__ == '__main__':

    logging.basicConfig(level=logging.INFO)

    # Files
    URL_CONFIG_FILE = "./config/urls.json"

    GEO_DATA_FILE = "./data/geographic.csv"
    GEO_DATA_FILE_DEMO = "./tests/test_data/geo_data.csv"

    SCRAPING_RESULT_FILE = "./data/raw_data.csv"
    SCRAPING_RESULT_FILE_DEMO = "./tests/test_data/scrap_output.csv"

    CLEANED_DATA_FILE = "./data/cleaned_data.csv"

    DATA_PROCESSOR_PARAMS = "./config/param_process.txt"

    VISUALIZER_BAR_PARAMS = "./config/param_bar.txt"
    VISUALIZER_SCATTER_PARAMS = "./config/param_scatter.txt"

    PREDICTION_INPUT = "./tests/test_data/in.csv"


    # Select population (thousands)
    POPULATION = 5

    # Web scraping
    scraper = Web_Scraper(webdriver.Firefox(),URL_CONFIG_FILE,GEO_DATA_FILE_DEMO,POPULATION,SCRAPING_RESULT_FILE_DEMO)
    scraper.start()
    logging.info('Web scraping: done')

    # Data cleaning
    data_c = Data_Cleaner(SCRAPING_RESULT_FILE,CLEANED_DATA_FILE)
    data_c.clean()
    logging.info('Data cleaning: done')

    # Data processing
    data_proc = Data_Processor(CLEANED_DATA_FILE,DATA_PROCESSOR_PARAMS)
    data_proc.exec()
    logging.info('Data processing: done')

    # Data visualisation
    data_visual = Data_Visualizer(VISUALIZER_BAR_PARAMS,VISUALIZER_SCATTER_PARAMS)
    #data_visual.exec("scatter")
    #data_visual.exec("bar")
    logging.info('Graph generation: done')

    # Linear Data Model
    print('---------------------------------------------------------------------')
    logging.info("Prediction en cours ...\n")
    lm = Linear_Model(CLEANED_DATA_FILE)
    res = lm.prediction("achat",PREDICTION_INPUT)
    print('---------------------------------------------------------------------')
    logging.info(" Estimation 1: "+str(res[0]))
    logging.info(" Estimation 1: "+str(res[1]))


