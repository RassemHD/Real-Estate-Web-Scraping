import pandas as pd
import numpy as np
import csv
import logging

class Data_Cleaner:

   def __init__(self, input_file, output_file):
        self.data = pd.read_csv(input_file, low_memory= False)
        self.output_file = output_file

   # première passe de néttoyage: suppression de doublons
   def first_cleaning(self):
      bool_s = self.data.duplicated()
      self.data = self.data[~bool_s]

      logging.info('cleaning raw data : step 1 OK')
      return self.data


   #sceonde passe de nettoyage : elimination des symboles et lettre des colonnes 
   # prix et taille et superficie 
   def second_cleaning(self,df):

      # prix étant en euro nous éliminant le symbole e , 
      # gérer les espaces et virgules
      df.prix = df.prix.apply(str)
      df.prix = df.prix.str.replace(r"\s+", "")
      df.prix = df.prix.str.extract(r'(\d*\.\d+|\d+)')

      # taille étant le nombre de chambre il s'agit de récuprer un entirer
      df.taille = df.taille.apply(str)
      df.taille = df.taille.str.extract(r'(\d*)')
      
      # superfcie étant exprimée en M2 
      df.superficie = df.superficie.apply(str)
      df.superficie = df.superficie.str.extract(r'(\d*\,\d+|\d+)')
      df.superficie = df.superficie.str.replace(r"(\,)",".")
      
      # type conversion
      df = df.astype({'taille': int, 'prix': float,'superficie': float}) 

      logging.info('cleaning raw data : step 2 OK')
      return df

   # trosième passe : elmination de valeurs dupliquée, aberrantes ...

   def third_cleaning(self,df):

      arr = np.array(df)

      def func(i):

         prix = arr[i,5]
         taille = arr[i,6]
         superficie = arr[i,7]

         if( (prix == float(taille)) and (prix >= 50)):
            arr[i,6] = np.nan

         if(taille > 15):
            arr[i,6] = np.nan
         
         if(superficie < 5):
            arr[i,7] = np.nan

      
      indexes = range(len(df))
      list(map(func,indexes))

      logging.info('cleaning raw data : step 3 OK')
      return pd.DataFrame(arr,columns=df.columns)
      
   def clean(self):
      # Suppression des doublons
      df = self.first_cleaning()
      # Suppression de texte inutile et typage
      df = self.second_cleaning(df)
      # Suppression de valeurs aberrantes
      df = self.third_cleaning(df)
      df.dropna
      # Sauvegarde sur le disque
      df.to_csv(self.output_file,index=False)
      
      
